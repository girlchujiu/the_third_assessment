import java.util.Scanner;
public class Main{
  public static void main(String args[]){
	    Scanner reader = new Scanner(System.in);
	    int n = reader.nextInt();
	    int[] a = new int[n];
	    int i=0;
	    int cnt=0;
	    for(i=0;i<n;i++){
	      a[i]=reader.nextInt();
	    }
	    for (i = 0; i < n - 1; i++){
		    a[i] = a[i + 1] - a[i];
	    }
	    for (i = 0; i < n - 1; i++){
		    if (i == 0){
			    System.out.printf("%d", a[0]);
		    }
		    else if (cnt == 3){
			    System.out.printf("\n");
			    System.out.printf("%d", a[i]);
			    cnt = 0;
		    }
		    else{
			    System.out.printf(" %d", a[i]);
		    }
		    cnt++;
	    }
	  }
	}